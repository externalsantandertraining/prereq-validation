# Prereqs

This README and the accompanying application help to ensure you have all the required software and that your laptop
is configured correctly to allow you to take part in Armakuni training. 

These instructions should be carried out for the machine you wish to use for training, **on the network the training will be given on.**

## What if I fail the prereqs? 

Don't worry, contact the trainer in advance of the session and extra time will be provided to help overcome any issues
discovered *before* the training starts.

## Software

All of this software should be installed:

- Java 8 (oracle or openJDK)
- Maven
- InteliJ (community or ultimate)
- Docker (for mac or machine) 
- A bash compatible shell
- git 

Mac users: You can use brew to install java and maven. 

```
brew install caskroom/versions/java8
brew install maven
```

## Verification

### Clone this repository Over SSH

This ensures you git installed and have working SSH access to bitbucket

### Run The Test Application

This ensures you have java and maven installed and that you are able to connect to maven central. 

```
mvn clean package
```

The output should contain `Tests run: 1, Failures: 0, Errors: 0, Skipped: 0` at the end of the run.

### Run The Test Application From InteliJ

This ensures inteliJ is correctly configured - Ie - It has picked up the JDK and maven locations. 

1. Open Intelij
1. Click "open project"
1. Select the projects directory
1. Navigate to: `src/test/java/com/example/demo/DemoApplicationTests.java`
1. Right Click on `DemoApplicationTests.java` and click 'RunDemoApplicationTests' 

The console should report green passing tests. 

### Run Docker

This ensures you are able to pull and run docker images. 

```
docker pull busybox
docker run -it --rm busybox
/ # echo hello world
```

